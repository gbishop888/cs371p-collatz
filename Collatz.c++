// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

//lazy cache that stores values as the code operates
int cache1[1000001];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);}

// ------------
// collatz_eval
// ------------
int collatz_helper (int i) {
    int count = 1;
    long int y = i;
    while (y != 1) {
        //checks the cache and skips ahead if a value is already in the cache
        if ((y < 1000001) && (cache1[y] > 0)) {
            return count + cache1[y] - 1;
        }
        if (y % 2 == 0) {
            y /= 2;
        } else {
            y *= 3;
            y++;
            y /= 2;
            count++;
        }
        count++;
    }
    return count;}

int collatz_eval (int i, int j) {
    // <your code>
    if (j < i) {
        swap(i, j);
    }
    int z = i;
    int largest = 0;
    int temp = 0;
    //calculate all the values
    while (z <= j) {
        temp = collatz_helper(z);
        //store values in the cache
        cache1[z] = temp;
        if (temp > largest) {
            largest = temp;
        }
        z++;
    }
    return largest;}


// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);}}
